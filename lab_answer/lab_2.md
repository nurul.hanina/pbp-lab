1. Apakah perbedaan antara JSON dan XML?
   JSON atau JavaScript Object Notation merupakan sebuah format file pertukaran data yang familiar bagi mata manusia
   dan mentransmit objek data dalam bentuk array. Berbeda dengan JSON, pengimplementasian XML atau eXtensible Markup
   Language merupakan format file dengan bentuk HTML tags dan tidak didesign untuk menampilkan data maka tidak begitu
   familiar bagi mata manusia yang langsung membaca potongan kodenya. Namun, kedua format file memiliki tujuan yang
   sama, yaitu untuk menyimpan dan mentransfer data. Perbedaan diantara keduanya dapat dilihat dari beberapa hal yaitu:
   - JSON untuk merepresentasikan objek, XML menggunakan tag untuk mewakili objek
   - JSON support array, XML tidak support array
   - JSON tidak menggunakan tag, XML menggunakan tag
   - JSON hanya menggunakan UTF-8 untuk encoding, XML lebih fleksibel karena support beberapa encoding
   - JSON tidak support comment, XML support comment

2. Apakah perbedaan antara HTML dan XML?
   Kepanjangan dari HTML adalah HyperText Markup Language, sedankan XML is an eXtensible Markup Language. Tujuan dari
   HTMl adalah untuk menampilkan data dan lebih fokus pada tampilannya, maka HTML meenggambarkan stuktur halaman web.
   Sedangkan XML bertujuan untuk menyimpan dan mentransfer informasi. Beberapa perbedaan diantara keduanya, antara lain:
   - HTML tidak case-sensitive, XML case-sensitive
   - HTML memiliki definisi tag tersendiri, XML memiliki tag yang menyesuaikan kebutuhan programmernya
   - HTML digunakan untuk menampilkan data, XML digunakan untuk mentransfer data
   - HTML bersifat static, XML bersifat dynamic
   - HTML adalah bahasa standar yang sederhana, XML adalah bahasa standar yang mendefinisikan bahasa lain

Source(s):
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.imaginarycloud.com/blog/json-vs-xml/
https://www.upgrad.com/blog/html-vs-xml/#HTML_vs_XML_Key_Differences
